package com.city.app.controller;

import com.city.app.dto.CityDto;
import com.city.app.entity.CityEntity;
import com.city.app.service.CityService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
@Api(tags = "Cities get/update/search APIs")
@Validated
public class CityController {

    private final CityService cityService;

    public CityController(CityService cityService) {
        this.cityService = cityService;
    }

    @ApiOperation(value = "Get paginated city list")
    @GetMapping(path = "/list")
    public Page<CityEntity> getCities(@RequestParam(value = "page_number", defaultValue = "1") int pageNumber,
                                      @RequestParam(value = "page_size", defaultValue = "10") int pageSize) {
        return cityService.getCitiesPage(pageNumber, pageSize);
    }

    @ApiOperation(value = "Search city by name")
    @GetMapping(path = "/city/{city_name}")
    public ResponseEntity<CityEntity> searchCityByName(@PathVariable(value = "city_name") String cityName) {
        return cityService.searchByName(cityName);
    }

    @ApiOperation(value = "Edit city name or photo url")
    @PostMapping(path = "/update")
    public ResponseEntity<CityEntity> updateCityNameOrPhoto(@RequestBody @Valid CityDto dto) {
        return cityService.updateCity(dto);
    }
}
