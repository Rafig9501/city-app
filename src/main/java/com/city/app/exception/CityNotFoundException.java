package com.city.app.exception;

public class CityNotFoundException extends RuntimeException {

    public CityNotFoundException(String message) {
        super(message);
    }

    public static void throwItWithMessage(String message) {
        throw new CityNotFoundException(message);
    }
}
