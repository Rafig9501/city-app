package com.city.app.repository;

import com.city.app.entity.CityEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CityRepository extends JpaRepository<CityEntity, Integer> {

    Optional<CityEntity> findByCityName(String cityName);
}
