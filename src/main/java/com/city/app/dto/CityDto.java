package com.city.app.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Validated
@Getter
@Setter
public class CityDto {

    @JsonProperty(value = "id", required = true)
    @NotNull(message = "Id cannot be null")
    private Integer id;

    @JsonProperty(value = "name")
    @Pattern(regexp="^[A-Za-z \\s\\-]*$|")
    private String name;

    @JsonProperty(value = "photo")
    @Pattern(regexp="^[A-Za-z \\s\\-]*$|")
    private String photo;
}
