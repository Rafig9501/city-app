package com.city.app.entity;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "cities")
@NoArgsConstructor
@Builder
@Setter
@AllArgsConstructor
@Getter
public class CityEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(name = "name")
    private String cityName;

    @Column(name = "photo")
    private String cityPhotoUrl;
}
