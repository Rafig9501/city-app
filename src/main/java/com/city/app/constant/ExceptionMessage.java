package com.city.app.constant;

public class ExceptionMessage {

    public static final String CITY_NOT_FOUND_ERROR = "There is no city with this id";
    public static final String SEARCH_CITY_ERROR = "Error in searchByName method in CityServiceImpl class : ";
    public static final String GET_CITIES_ERROR = "Error in searchByName method in CityServiceImpl class : ";
    public static final String DTO_VALIDATION_ERROR = "Input validation has been failed";
    public static final String METHOD_NOT_SUPPORTED = "HTTP method is not supported";
    public static final String BAD_REQUEST_EXCEPTION = "Request body validation has failed due to some reasons";
    public static final String INTERNAL_ERROR = "Internal error has been occurred";

    private ExceptionMessage() {
    }
}

