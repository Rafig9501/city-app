package com.city.app.service.impl;

import com.city.app.constant.ExceptionMessage;
import com.city.app.dto.CityDto;
import com.city.app.entity.CityEntity;
import com.city.app.exception.CityNotFoundException;
import com.city.app.repository.CityRepository;
import com.city.app.service.CityService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
@Slf4j
public class CityServiceImpl implements CityService {

    private final CityRepository cityRepository;

    public CityServiceImpl(CityRepository cityRepository) {
        this.cityRepository = cityRepository;
    }

    @Override
    public Page<CityEntity> getCitiesPage(int pageNumber, int pageSize) {
        try {
            Pageable pageable = PageRequest.of(pageNumber - 1, pageSize - 1);
            return cityRepository.findAll(pageable);
        } catch (Exception e) {
            log.error(ExceptionMessage.GET_CITIES_ERROR + e.getMessage());
            return Page.empty();
        }
    }

    @Override
    public ResponseEntity<CityEntity> searchByName(String name) {
        try {
            return cityRepository.findByCityName(name).map(cityEntity ->
                    new ResponseEntity<>(cityEntity, HttpStatus.OK)).orElseGet(() ->
                    new ResponseEntity<>(null, HttpStatus.NO_CONTENT));
        } catch (Exception e) {
            log.error(ExceptionMessage.SEARCH_CITY_ERROR + e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<CityEntity> updateCity(CityDto dto) {
        var byId = cityRepository.findById(dto.getId());
        if (byId.isEmpty()) {
            CityNotFoundException.throwItWithMessage(ExceptionMessage.CITY_NOT_FOUND_ERROR);
        }
        var city = byId.get();
        if (dto.getName() != null) {
            city.setCityName(dto.getName());
        }
        if (dto.getPhoto() != null) {
            city.setCityPhotoUrl(dto.getPhoto());
        }
        return new ResponseEntity<>(cityRepository.save(city), HttpStatus.OK);
    }
}