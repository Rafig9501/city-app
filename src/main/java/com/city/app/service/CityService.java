package com.city.app.service;

import com.city.app.dto.CityDto;
import com.city.app.entity.CityEntity;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public interface CityService {

    Page<CityEntity> getCitiesPage(int pageNumber, int pageSize);

    ResponseEntity<CityEntity> searchByName(String name);

    ResponseEntity<CityEntity> updateCity(CityDto dto);
}
