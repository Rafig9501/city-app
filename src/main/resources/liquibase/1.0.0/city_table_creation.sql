create table if not exists cities
(
    id bigserial,
    name varchar(32),
    photo varchar
);

create unique index if not exists cities_id_uindex
    on cities (id);

alter table cities
    add constraint cities_pk
        primary key (id);